
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class App {
    public static void main(String[] args) throws Exception {
        subTask1("Devcamp JAVA exercise");
        subTask2("word", "drow");
        subTask3("Java");
        subTask4("word");
        subTask5("adc1");
        subTask6("java");
        subTask7("1234");
        subTask8('a', 'e');
        subTask9("I am developer");
        subTask10("abc");
    }

    public static void subTask1(String str) {
        Map<Character, Integer> charCountMap = new HashMap<>();
        boolean hasDuplicates = false;
        String newString = str.replaceAll("\\s", "");
        for (char c : newString.toCharArray()) {
            c = Character.toLowerCase(c);
            charCountMap.put(c, charCountMap.getOrDefault(c, 0) + 1);
        }

        System.out.println(newString);
        for (Map.Entry<Character, Integer> entry : charCountMap.entrySet()) {
            if (entry.getValue() > 1) {
                System.out
                        .println("Ký tự '" + entry.getKey() + "' xuất hiện " + entry.getValue() + " lần trong chuỗi.");
                hasDuplicates = true;
            }
        }
        if (!hasDuplicates) {
            System.out.println("NO");
        }
    }

    public static void subTask2(String str1, String str2) {
        if (str1.length() != str2.length()) {
            System.out.println("KO");
            return;
        }

        int length = str1.length();
        for (int i = 0; i < length; i++) {
            if (str1.charAt(i) != str2.charAt(length - 1 - i)) {
                System.out.println("KO");
                return;
            }
        }

        System.out.println("OK");
    }

    public static void subTask3(String str) {
        Map<Character, Integer> charCountMap = new HashMap<>();

        for (char c : str.toCharArray()) {
            charCountMap.put(c, charCountMap.getOrDefault(c, 0) + 1);
        }

        for (char c : str.toCharArray()) {
            if (charCountMap.get(c) == 1) {
                System.out.println("Ký tự đầu tiên chỉ xuất hiện một lần trong chuỗi là: " + c);
                return;
            }
        }

        System.out.println("NO");
    }

    public static void subTask4(String str) {
        StringBuilder sb = new StringBuilder(str);
        sb.reverse();
        System.out.println(sb);
    }

    public static void subTask5(String str) {
        for (char c : str.toCharArray()) {
            if (Character.isDigit(c)) {
                System.out.println("true");
            }
        }
        System.out.println("false");
    }

    public static void subTask6(String str) {
        int count = 0;
        String vowels = "aeiouAEIOU";

        for (char c : str.toCharArray()) {
            if (vowels.indexOf(c) != -1) {
                count++;
            }
        }
        System.out.println(count);

    }

    public static void subTask7(String str) {
        int value = Integer.parseInt(str);
        System.out.println("Value: " + value);

    }

    public static void subTask8(char oldChar, char newChar) {
        String str = "devcamp java";
        str.replace(oldChar, newChar);
        System.out.println("String: " + str);
    }

    public static void subTask9(String str) {
        String[] words = str.split(" ");
        StringBuilder reversed = new StringBuilder();

        for (int i = words.length - 1; i >= 0; i--) {
            reversed.append(words[i]);
            if (i > 0) {
                reversed.append(" ");
            }
        }

        System.out.println("Reverse String: " + reversed.toString());
    }

    public static void subTask10(String str) {
        StringBuilder reversed = new StringBuilder(str).reverse();
        boolean isEqual = str.equals(reversed.toString());
        System.out.println("Is Equal String: " + isEqual);
    }

}
